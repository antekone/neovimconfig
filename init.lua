g_debug = true
options = {}

function trace(arg) if g_debug == true then print("[trace] " .. tostring(arg)) end end

function query_root_dir()
    local self = vim.fn.expand("<sfile>:p")
    local resolved = vim.fn.resolve(self)
    return vim.fn.fnamemodify(resolved, ":h")
end

function file_exists(path)
    if type(path) ~= "string" then return false end
    -- trace(path)
    local fp = io.open(path, "rb")
    -- trace(fp)
    if fp ~= nil then
        io.close(fp)
        return true
    end
    return false
end

function validate_files()
    local root = query_root_dir()
    -- trace(root)
    if not file_exists(root .. "/plug.vim") then
        print("Missing " .. root .. "/plug.vim")
        return false
    end
    return true
end

-- "linux", "windows", "macos"
function current_os()
    local linux = "linux"
    local macos = "macos"
    local windows = "windows"
    local unknown = "unknown"
    local wsl = "wsl"
    local normal = "normal"

    if file_exists("c:/windows/system32/kernel32.dll") then
        -- Normal Windows
        return windows, normal
    elseif file_exists("/c/windows/system32/kernel32.dll") then
        -- Windows under WSL
        return windows, wsl
    elseif file_exists("/usr/bin/sw_vers") then
        return macos, normal
    elseif file_exists("/etc/group") then
        -- Normal Linux
        return linux, normal
    end

    return unknown, unknown
end

function nn(from, to)
    local keymap_opts = { noremap = true }
    vim.api.nvim_set_keymap('n', from, to, keymap_opts)
end

function vn(from, to)
    local keymap_opts = { noremap = true }
    vim.api.nvim_set_keymap('v', from, to, keymap_opts)
end

function tn(from, to)
    local keymap_opts = { noremap = true }
    vim.api.nvim_set_keymap('t', from, to, keymap_opts)
end

function ino(from, to)
    local keymap_opts = { noremap = true }
    vim.api.nvim_set_keymap('i', from, to, keymap_opts)
end

os_family, os_subtype = current_os()
if os_family == 'unknown' or os_subtype == 'unknown' then
    print("Unsupported OS, sorry")
    return false
end

function is_normal_windows() return os_family == 'windows' and os_subtype == 'normal' end
function is_macos() return os_family == 'macos' end

if not validate_files() then
    return false
end

vim.cmd("set rtp^="..query_root_dir())
require("plugins")
require("functions")

vim.cmd("colors tender")

if os.getenv("TERM_PROGRAM") ~= "Apple_Terminal" then
    -- "termguicolors" are not supported by AppleTerminal,
    -- but iTerm doesn't have any problems with it; so we
    -- just disable it for AppleTerminal.
    vim.cmd("set termguicolors")
end

vim.cmd("set nu")
vim.cmd("set cursorline")

--
-- Keymap
--

nn('L', '$')
nn('H', '^')
nn('j', 'gj')
nn('k', 'gk')
vn('L', '$')
vn('H', '^')

-- Ctrl+backspace == eat a word
ino('<C-BS>', '<C-w>')
ino('<C-h>', '<C-w>')

-- Small script to begin insertion in line above.
ino('<C-o>', '<esc>O')

-- Use Q to run default macro @q. I never use real Q anyway.
nn('Q', '@q')

-- Use <leader-A> to switch between header/implementation.
nn('<leader>A', ':FSHere<cr>')

if is_normal_windows() then
    run_script = "make.bat"
else
    run_script = "./.make.sh"
end

nn('<F3>', '<cmd>lua check_indentation_integrity()<cr>')
nn('<F4>', '<cmd>lua clean_file()<cr>')
nn('<F9>', '<cmd>AsyncRun '..run_script..'<cr>')
nn('<F12>', '<cmd>lua clear_screen()<cr>')
nn('_', '<cmd>lua toggle_maximize()<cr>')
nn('<leader>m', '<cmd>AsyncRun '..run_script..'<cr>')

nn('<leader>T', '<cmd>lua toggle_tab_visibility()<cr>')
nn('<leader>e', '<cmd>lua toggle_quickfix()<cr>')

-- Don't use cursors to jump over the buffer. Instead use them to resize
-- current windows.
--
-- Use Shift-Arrows to resize windows faster.
nn('<Up>', '<C-W>-')
nn('<Down>', '<C-W>+')
nn('<Left>', '<C-W><')
nn('<Right>', '<C-W>>')
nn('<S-Up>', '4<C-W>-')
nn('<S-Down>', '4<C-W>+')
nn('<S-Left>', '4<C-W><')
nn('<S-Right>', '4<C-W>>')

-- Move through different windows by using Ctrl+HJKL
nn('<C-L>', '<C-W>l')
nn('<C-H>', '<C-W>h')
nn('<C-J>', '<C-W>j')
nn('<C-K>', '<C-W>k')

-- Easily exit the terminal.
tn('<C-L>', '<C-\\><C-n><C-W>l')
tn('<C-H>', '<C-\\><C-n><C-W>h')
tn('<C-K>', '<C-\\><C-n><C-W>k')
tn('<C-J>', '<C-\\><C-n><C-W>j')

-- Don't forget the selection when shifting blocks of text.
vn('>', '>gv')
vn('<', '<gv')

-- Use "jk" to exit insert mode. <esc> should also work, but jk is faster.
-- It makes problems with some words like "jajko" but those are rare.
--
-- Use JK to exit insert mode and save the file quickly.
ino('jk', '<esc>')
ino('JK', '<esc>:w<cr>')
ino('<leader>j', 'jk')
ino('<leader>J', 'JK')

-- Exit visual mode with <return>.
vn('<return>', '<esc>')

-- I like to use tabs in vim sessions, so there are some keybindings for them.
--
-- Create a new tab with <leader-t>.
nn('<leader>t', ':tabnew<cr>')

-- Jump to left/right tab with <leader-a> and <leader-s> respectively.
nn('<leader>a', ':tabprev<cr>')
nn('<leader>s', ':tabnext<cr>')

-- Close tab or quit vim with <leader-q>.
nn('<leader>q', ':q<cr>')

-- Use <leader-space> to clear highlighting after a search.
nn('<leader><space>', ':nohl<cr>')

-- set leader
vim.g.mapleader = "\\"

-- Wrap lines at word boundary instead of character boundary.
vim.cmd("set linebreak")

-- r: Automatically insert comment leader when necessary.
-- q: Insert comment leader when using `gq`.
-- j: Remove comment leader when joining lines.
vim.cmd("set formatoptions+=rqj")

-- spaces are better than tabs
vim.cmd("set expandtab")

-- Use tabstop 4, shiftwidth 4. Those options will be changed later for
-- respective formats (i.e. ruby uses ts=2, sw=2)
vim.cmd("set ts=4")
vim.cmd("set sw=4")

-- I don't live in UK, so I'm not using additional space after punctuation
-- character.
vim.cmd("set nojoinspaces")

-- Don-t insert BOM when saving files in UTF encoding
vim.cmd("set nobomb")

-- Allow positioning cursor on all places on the screen
vim.cmd("set virtualedit=all")

-- Briefly show matching bracket after insertion
vim.cmd("set showmatch")

-- Always show at least 3 lines when scrolling up/down, and 5 columns when
-- scrolling left/right
vim.cmd("set scrolloff=3")
vim.cmd("set sidescrolloff=5")

-- Airline configuration.
vim.cmd("let g:airline#extensions#tabline#formatter = 'unique_tail'")
vim.cmd("let g:airline#extensions#tabline#enabled = 1")
vim.cmd("let g:airline#extensions#tabline#buffer_idx_mode = 1")

-- Don't change cursor to beam in insert mode.
vim.cmd("set guicursor=i:block")

-- React properly on :cc
vim.cmd("set switchbuf=useopen,usetab,newtab")

vim.api.nvim_create_autocmd({ "BufReadPost" }, { 
    callback = function()
        check_indentation_integrity()
    end
})

-- Remember last file position.
vim.api.nvim_create_autocmd({ "BufReadPost" }, {
    pattern = { "*" },
    callback = function()
        vim.api.nvim_exec('silent! normal! g`"zv', false)
    end
})

-- Global project compilation settings
--
-- Use .make.sh from current dir when trying to compile. This script will
-- contain project-specific build command.
vim.cmd("set makeprg="..run_script..".sh")

vim.opt.mouse = nil

-- Vim-pick settings
nn('<leader>pf', '<Plug>(PickerEdit)')
nn('<leader>pt', '<Plug>(PickerBufferTag)')
nn('<leader>pT', '<Plug>(PickerTag)')
nn('<leader>pb', '<Plug>(PickerBuffer)')
-- Alias for \pb -- \o is easier to type ;)
nn('<leader>o', '<Plug>(PickerBuffer)')

-- Further vim-pick configuration needs to be done in vim-local file:
vim.g.picker_custom_find_executable = 'git'
vim.g.picker_custom_find_flags = 'ls-files --recurse-submodules --exclude-standard --full-name --cached'

if is_normal_windows() then
    vim.g.picker_selector_executable = 'fzf'
    vim.g.picker_selector_flags = '--no-mouse --layout=reverse-list'
else
    vim.g.picker_selector_executable = 'fzy'
    vim.g.picker_selector_flags = ''
end

local root_dir = query_root_dir()
if file_exists(root_dir .. "/lua/local.lua") then
    require("local")
else
    print("not including local")
end
