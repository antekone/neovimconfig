local vim = vim
local root = query_root_dir()

vim.cmd("source "..root.."/plug.vim")
local Plug = vim.fn['plug#']
vim.call('plug#begin', root.."/plugged")

Plug 'skywind3000/asyncrun.vim'
--Plug 'neovim/nvim-lspconfig'

-- Default settings
Plug 'tpope/vim-sensible'
-- Git client
Plug 'tpope/vim-fugitive'
-- Easy alignment
Plug 'junegunn/vim-easy-align'
-- File navigator
Plug 'scrooloose/nerdtree'
-- Line commenter
Plug 'scrooloose/nerdcommenter'
-- Fuzzy file finder
Plug 'kien/ctrlp.vim'
-- H/CPP switcher
Plug 'antekone/vim-fswitch'
-- integration with gnu global/gtags
-- my fork prevents auto-jumping to first result after showing quickfix
-- window.
Plug 'antekone/gtags.vim'
-- easymotion (\\w, \\f)
Plug 'Lokaltog/vim-easymotion'
-- the silver searcher
Plug 'rking/ag.vim'
-- bracketed paste support
Plug 'ConradIrwin/vim-bracketed-paste'
-- better status line
Plug 'vim-airline/vim-airline'
-- integration with tmux
Plug 'edkolev/tmuxline.vim'
-- editorconfig support
Plug 'editorconfig/editorconfig-vim'
-- Vim-picker
Plug('srstevenson/vim-picker', { branch = 'main' })

-- colors
Plug 'jacoborus/tender.vim'

-- syntax
Plug 'tfnico/vim-gradle'
Plug 'derekwyatt/vim-scala'
Plug 'rust-lang/rust.vim'
Plug 'sirtaj/vim-openscad'
Plug 'aklt/plantuml-syntax'
Plug 'udalov/kotlin-vim'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'jdonaldson/vaxe'
Plug 'dart-lang/dart-vim-plugin'

vim.call('plug#end')

