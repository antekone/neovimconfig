function toggle_tab_visibility()
    if vim.g.ttv == true then
        vim.g.ttv = false
        vim.cmd("set nolist")
        vim.cmd("match") -- clear matches
        print("TabVisibility off")
    else
        vim.g.ttv = true
        vim.cmd("set list")
        -- tab:xy, eol:c
        vim.cmd("set listchars=tab:>-,eol:$,trail:-")
        vim.cmd("match errorMsg /\\s\\+$/")
        print("TabVisibility on")
    end
end

function toggle_quickfix()
    for _, i in pairs(vim.fn.tabpagebuflist()) do
        local tab = vim.fn.getbufvar(i, "&buftype")
        if tab == "quickfix" then
            vim.cmd("cclose")
            return
        end
    end

    -- force open quickfix in the whole bottom area
    vim.cmd("botright copen")
end

function clear_screen()
    vim.cmd("cclose")
end

toggle_maximize_state = {}

function toggle_maximize()
    local cur = vim.api.nvim_get_current_win()
    local prev_state = toggle_maximize_state[cur]
    if prev_state ~= nil then
        toggle_maximize_state[cur] = nil

        vim.cmd(cur.."resize "..prev_state.h)
        vim.cmd("vert "..cur.."resize "..prev_state.w)
    else
        toggle_maximize_state[cur] = {
            w = vim.fn.winwidth(cur),
            h = vim.fn.winheight(cur)
        }

        vim.cmd(cur.."resize 9999")
        vim.cmd("vert "..cur.."resize 9999")
    end
end

function check_indentation_integrity()
    local line_count = vim.api.nvim_buf_line_count(0)
    if line_count == 0 or line_count > 100000 then return end

    local tab_lines = 0
    local space_lines = 0
    local indent_suffix = 0

    for line_idx = 0, line_count - 1, 1 do
        local line = vim.api.nvim_buf_get_lines(0, line_idx, line_idx + 1, true)[1]
        local first = line:sub(1, 1)
        local last = line:sub(-1, -1)

        if first == "\t" then
            tab_lines = tab_lines + 1
        elseif first == " " then
            space_lines = space_lines + 1
        end

        if last == "\t" or last == " " then
            indent_suffix = indent_suffix + 1
        end
    end

    print("Tabs: "..tab_lines..", spaces: "..space_lines..", space at end: "..indent_suffix)
end

function clean_file()
    vim.cmd("retab")
    vim.api.nvim_input("1GVG><lt><return>1G")
    vim.cmd("%s/\\s\\+$//e")

    vim.g.ttv = false
    toggle_tab_visibility()
    check_indentation_integrity()
end
